import {PrismaClient} from "@prisma/client";
import { GameStatus, GamePlatform, GamePlatformGeneration } from "@prisma/client";

const game = [
  {
    name: "FIFA 22",
    description: "Just another FIFA game.",
    platform: [
      GamePlatform.PLAYSTATION
    ],
    platformGeneration: [
      GamePlatformGeneration.PS5
    ],
    status: GameStatus.ACTIVE,
    createdDate: new Date(),
    modifiedDate: new Date()
  }
];
export default async (prisma: PrismaClient) => {
  try {
    await prisma.game.deleteMany();
    console.log('Deleted records in Game collection');

    await prisma.game.createMany({
      data: game
    });
  } catch (err) {
    console.error('Game seeding failed...');
    console.warn(err);
  }
}
