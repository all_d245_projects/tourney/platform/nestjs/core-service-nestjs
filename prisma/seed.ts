import { PrismaClient } from '@prisma/client'
import loadGameSeed from './fixtures/game';

const prisma = new PrismaClient()

const seed = async () => {
  try {
    const gameSeed = loadGameSeed(prisma);

    await Promise.all([gameSeed]);

    console.log('Seed complete.');
  } catch (e) {
    console.error(e);
    process.exit(1);
  } finally {
    await prisma.$disconnect();
  }
};

seed();