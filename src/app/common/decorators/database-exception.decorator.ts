import { RpcException } from '@nestjs/microservices';
import {
  PrismaClientKnownRequestError,
  PrismaClientRustPanicError,
  PrismaClientUnknownRequestError,
  PrismaClientValidationError,
} from '@prisma/client/runtime/library';

export const DatabaseFailureHandler = (target: Function) => {
  for (const propertyName of Object.getOwnPropertyNames(target.prototype)) {
    const descriptor = Object.getOwnPropertyDescriptor(
      target.prototype,
      propertyName,
    );
    const isMethod = descriptor?.value instanceof Function;
    if (!isMethod) continue;

    const originalMethod = descriptor.value;

    descriptor.value = new Proxy(originalMethod, {
      apply: async function (target, thisArg, args) {
        try {
          return await target.apply(thisArg, args);
        } catch (error) {
          if (error instanceof PrismaClientKnownRequestError) {
            switch (error.code) {
              case 'P2002':
                throw new RpcException(
                  `PRISMA DB CONSTRAINT ERROR: ${error?.meta?.target}`,
                );
              default:
                throw new RpcException('PRISMA DB ERROR');
            }
          } else if (
            error instanceof PrismaClientRustPanicError ||
            error instanceof PrismaClientUnknownRequestError ||
            error instanceof PrismaClientValidationError
          ) {
            throw new RpcException(`PRISMA DB ERROR: ${error.message}`);
          }
        }
      },
    });

    Object.defineProperty(target.prototype, propertyName, descriptor);
  }
};
