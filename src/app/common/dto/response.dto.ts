import {
  IsArray,
  IsOptional,
  IsString,
  Validate,
  ValidatorConstraintInterface,
} from 'class-validator';

class IsValidResult implements ValidatorConstraintInterface {
  validate(value: any) {
    return (
      typeof value === 'object' ||
      value === 'array' ||
      typeof value === 'string'
    );
  }

  defaultMessage() {
    return '($value) must be object or string';
  }
}

export class ControllerResponseDto {
  @IsString()
  @IsOptional()
  message?: string | undefined;

  @IsArray()
  errors?: [] | undefined;

  @Validate(IsValidResult)
  result: string | object;
}
