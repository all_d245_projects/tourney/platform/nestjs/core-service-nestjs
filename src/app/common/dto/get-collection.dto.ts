import { IsOptional, IsPositive } from 'class-validator';

export class GetCollectionDto {
  @IsOptional()
  @IsPositive()
  limit?: number;

  @IsOptional()
  filter?: Record<string, unknown>;
}
