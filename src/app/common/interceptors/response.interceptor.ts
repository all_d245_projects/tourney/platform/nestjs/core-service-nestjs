import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_INTERCEPTOR } from '@nestjs/core';

@Injectable()
export class ResponseInterceptor<T>
  implements NestInterceptor<T, Record<string, unknown>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Record<string, unknown>> {
    return next.handle().pipe(
      map(data => ({
        message: data?.message || 'ACK',
        result: data?.result || {},
      })),
    );
  }
}

export const ResponseInterceptorProvider = {
  provide: APP_INTERCEPTOR,
  useClass: ResponseInterceptor,
};
