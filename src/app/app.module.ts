import { Module } from '@nestjs/common';
import ConfigModuleConfig from '@/config/modules/config-module.config';
import { LoggerModule } from '@/app/modules/logger/logger.module';
import { NatsClientModule } from '@/app/modules/nats/nats-client.module';
import { ResponseInterceptorProvider } from '@/app/common/interceptors/response.interceptor';
import { GamesModule } from '@/app/modules/games/games.module';

@Module({
  imports: [ConfigModuleConfig, LoggerModule, NatsClientModule, GamesModule],
  providers: [ResponseInterceptorProvider],
})
export class AppModule {}
