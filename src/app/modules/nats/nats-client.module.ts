import { Global, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory } from '@nestjs/microservices';

const LoggerServiceNatsClient = {
  provide: 'NATS_LOGGER_SERVICE_CLIENT',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    const natsModuleOptions = configService.get('nats.loggerService');
    return ClientProxyFactory.create(natsModuleOptions);
  },
};

@Global()
@Module({
  providers: [LoggerServiceNatsClient],
  exports: [LoggerServiceNatsClient],
})
export class NatsClientModule {}
