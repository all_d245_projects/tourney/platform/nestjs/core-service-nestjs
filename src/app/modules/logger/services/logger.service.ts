import { ConsoleLogger, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

interface LogData {
  logLevel: 'error' | 'info' | 'warning' | 'debug';
  message: string;
  meta: any;
}

@Injectable()
export class LoggerService extends ConsoleLogger {
  constructor(
    @Inject('NATS_LOGGER_SERVICE_CLIENT')
    private loggerServiceNatsClient: ClientProxy,
  ) {
    super();
  }
  log(message: any, context?: string, meta?: any) {
    super.log(message, context);
    this.emitLogMessage({
      logLevel: 'info',
      message,
      meta: { ...meta, context },
    });
  }

  warn(message: any, context?: string, meta?: any) {
    super.log(message, context);
    this.emitLogMessage({
      logLevel: 'warning',
      message,
      meta: { ...meta, context },
    });
  }

  debug(message: any, context?: string, meta?: any) {
    super.log(message, context);
    this.emitLogMessage({
      logLevel: 'debug',
      message,
      meta: { ...meta, context },
    });
  }

  verbose(message: any, context?: string, meta?: any) {
    super.log(message, context);
    this.emitLogMessage({
      logLevel: 'debug',
      message,
      meta: { ...meta, context },
    });
  }

  error(message: any, stack?: string, context?: string): void {
    super.error(message, stack, context);
    this.emitLogMessage({
      logLevel: 'error',
      message,
      meta: { stack, context },
    });
  }

  private emitLogMessage(logData: LogData): void {
    this.loggerServiceNatsClient.emit('LOGGER_SERVICE.log', {
      ...logData,
      ...{
        service: 'CORE_SERVICE',
        timestamp: new Date(this.getTimestamp()).toISOString(),
      },
    });
  }
}
