import { Module } from '@nestjs/common';
import { GamesController } from '@/app/modules/games/controllers/games.controller';
import { GamesService } from '@/app/modules/games/services/games.service';
import { PrismaService } from '@/app/common/services/prisma/prisma.service';

@Module({
  providers: [GamesService, PrismaService],
  controllers: [GamesController],
})
export class GamesModule {}
