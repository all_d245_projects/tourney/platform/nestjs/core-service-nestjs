import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ControllerResponseDto } from '@/app/common/dto/response.dto';
import { GamesService } from '@/app/modules/games/services/games.service';
import { CreateGameDTO } from '@/app/modules/games/dto/games.dto';

export enum GamesResponseMessage {
  GET_GAMES_OK = 'get-games OK',
  CREATE_GAME_OK = 'create-game OK',
}

@Controller()
export class GamesController {
  constructor(private readonly gamesService: GamesService) {}

  @MessagePattern('get-games')
  async getGames(): Promise<ControllerResponseDto> {
    const games = await this.gamesService.getGames();

    return {
      message: GamesResponseMessage.GET_GAMES_OK,
      result: games,
    };
  }

  @MessagePattern('create-game')
  async createGame(
    @Payload() data: CreateGameDTO,
  ): Promise<ControllerResponseDto> {
    const game = await this.gamesService.createGame(data);

    return {
      message: GamesResponseMessage.CREATE_GAME_OK,
      result: game,
    };
  }
}
