import { Injectable } from '@nestjs/common';
import { PrismaService } from '@/app/common/services/prisma/prisma.service';
import { Game } from '@prisma/client';
import { CreateGameDTO } from '@/app/modules/games/dto/games.dto';
import { DatabaseFailureHandler } from '@/app/common/decorators/database-exception.decorator';

@DatabaseFailureHandler
@Injectable()
export class GamesService {
  constructor(private prismaService: PrismaService) {}

  async getGames(): Promise<Game[]> {
    return await this.prismaService.game.findMany();
  }

  async createGame(gameData: CreateGameDTO): Promise<Game> {
    return await this.prismaService.game.create({
      data: {
        ...gameData,
        createdDate: new Date(),
      },
    });
  }
}
