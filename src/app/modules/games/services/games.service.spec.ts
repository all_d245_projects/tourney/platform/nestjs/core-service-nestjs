import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '@/app/common/services/prisma/prisma.service';
import { GameTestHelper } from '@test/helpers/game-test.helper';
import { mockDeep, DeepMockProxy } from 'jest-mock-extended';
import {
  GamePlatform,
  GamePlatformGeneration,
  GameStatus,
  PrismaClient,
} from '@prisma/client';
import { plainToInstance } from 'class-transformer';
import { GamesService } from '@/app/modules/games/services/games.service';
import { CreateGameDTO } from '@/app/modules/games/dto/games.dto';

describe('GamesService', () => {
  let gamesService: GamesService;
  let prisma: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GamesService, PrismaService],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    gamesService = module.get(GamesService);
    prisma = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(gamesService).toBeDefined();
  });

  describe('getGames', () => {
    it('should return an array of games', async () => {
      const expectedResult = GameTestHelper.getMockGames();

      prisma.game.findMany.mockResolvedValueOnce(expectedResult);

      expect(gamesService.getGames()).resolves.toBe(expectedResult);
    });
  });

  describe('createGame', () => {
    it('should return a single game', async () => {
      const expectedResult = GameTestHelper.getMockGames(1)[0];
      const gameCreateDto = plainToInstance(CreateGameDTO, {
        name: 'FIFA 22',
        description: 'Just another FIFA game.',
        platform: [GamePlatform.PLAYSTATION],
        platformGeneration: [GamePlatformGeneration.PS5],
        status: GameStatus.ACTIVE,
      });

      prisma.game.create.mockResolvedValueOnce(expectedResult);

      expect(gamesService.createGame(gameCreateDto)).resolves.toBe(
        expectedResult,
      );
    });
  });
});
