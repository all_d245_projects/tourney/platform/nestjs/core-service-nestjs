import { GetCollectionDto } from '@/app/common/dto/get-collection.dto';
import { IsArray, IsEnum, IsString } from 'class-validator';
import {
  GamePlatform,
  GamePlatformGeneration,
  GameStatus,
} from '@prisma/client';

export class GetGameDTO extends GetCollectionDto {}

export class CreateGameDTO {
  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsArray()
  @IsEnum(GamePlatform, { each: true })
  platform: GamePlatform[];

  @IsArray()
  @IsEnum(GamePlatformGeneration, { each: true })
  platformGeneration: GamePlatformGeneration[];

  @IsEnum(GameStatus)
  status: GameStatus;
}
