import { NestFactory } from '@nestjs/core';
import { AppModule } from '@/app/app.module';
import { ConfigService } from '@nestjs/config';
import { Logger } from '@nestjs/common';
import { MicroserviceOptions } from '@nestjs/microservices';
import { LoggerService } from '@/app/modules/logger/services/logger.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });
  const configService = app.get(ConfigService);
  const coreServiceNatsConfig = configService.get('nats.coreService');

  app.useLogger(app.get(LoggerService));

  app.connectMicroservice<MicroserviceOptions>(coreServiceNatsConfig, {
    inheritAppConfig: true,
  });

  await app.startAllMicroservices();

  Logger.log(
    `NATS ready and listening on ${coreServiceNatsConfig.options.servers}`,
    'AppBootstrap',
  );
}
bootstrap();
