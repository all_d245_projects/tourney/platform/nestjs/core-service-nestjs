import { registerAs } from '@nestjs/config';
import { Environment } from '@/app/common/validation/environmentVariable.validation';
import { ValidationError } from 'class-validator';
import { RpcException } from '@nestjs/microservices';

export default registerAs('app', () => ({
  environment: process.env.NODE_ENV,
  globalValidationPipeOptions: {
    exceptionFactory: (validationErrors: ValidationError[]) => {
      const errors: string[] = [];

      validationErrors.forEach(error => {
        error
          .toString(false, true, '', true)
          .split('\n')
          .forEach(val => {
            if (val !== '') {
              errors.push(val.slice(3).trim());
            }
          });
      });

      throw new RpcException({
        message: 'Validation Error',
        result: errors,
      });
    },
    whitelist: true,
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    transform: true,
    disableErrorMessages: process.env.NODE_ENV === Environment.Production,
    validationError: {
      value: true,
      target: true,
    },
  },
}));
