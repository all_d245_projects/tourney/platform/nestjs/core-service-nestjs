import { Environment } from '@/app/common/validation/environmentVariable.validation';
import { validate } from '@/app/common/validation/environmentVariable.validation';
import appConfig from '@/config/registered/app.config';
import natsConfig from '@/config/registered/nats.config';
import { ConfigModule } from '@nestjs/config';

function getConfigOptionsFromEnvironmentContext(): Record<string, unknown> {
  switch (process.env.NODE_ENV) {
    case Environment.Production:
    case Environment.Development:
    case Environment.Local:
      return {
        cache: true,
        envFilePath: '.env',
      };

    case Environment.Test:
      return {
        cache: false,
        envFilePath: '.env.test',
      };

    default:
      throw new Error('could not read NODE_ENV from the environment.');
  }
}

export default ConfigModule.forRoot({
  isGlobal: true,
  load: [natsConfig, appConfig],
  validate,
  ...getConfigOptionsFromEnvironmentContext(),
});
