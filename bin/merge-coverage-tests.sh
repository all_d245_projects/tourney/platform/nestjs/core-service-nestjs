#!/bin/bash

# Create merged coveraage report directory
mkdir -p coverage/merged

# Clear directory if not empty
if [ ! -z "$(ls -A coverage/merged)" ]; then
   rm -r coverage/merged/*
fi

# check if unit & e2e tests have been created
if [[ ! -f coverage/unit/coverage-final.json && ! -f coverage/e2e/coverage-final.json ]]; then
    echo "Unit & E2E coverage files not found!"
    exit 1
fi

# copy unit & e2e json tests into merged directory
cp coverage/unit/coverage-final.json coverage/merged/unit-coverage-final.json
cp coverage/e2e/coverage-final.json coverage/merged/e2e-coverage-final.json

# execute nyc to merge tests
npx nyc merge coverage/merged coverage/merged/merged-coverage.json

# create report
npx nyc report -t coverage/merged --report-dir coverage/merged --reporter=lcov --reporter=cobertura --reporter=text-summary