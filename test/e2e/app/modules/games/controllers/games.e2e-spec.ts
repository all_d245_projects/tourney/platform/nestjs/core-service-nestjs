import { TestingModule } from '@nestjs/testing';
import { AppTestHelper } from '@test/helpers/app-test.helper';
import { Observable, catchError, of } from 'rxjs';
import { PrismaService } from '@/app/common/services/prisma/prisma.service';
import { GameTestHelper } from '@test/helpers/game-test.helper';
import {
  GamePlatform,
  GamePlatformGeneration,
  GameStatus,
} from '@prisma/client';

describe('GamesController (e2e)', () => {
  let appTestHelper: AppTestHelper;
  let prisma: PrismaService;

  beforeEach(async () => {
    appTestHelper = new AppTestHelper();

    const testingModule: TestingModule =
      await AppTestHelper.createE2eTestingModule();

    await appTestHelper.init(testingModule);

    prisma = appTestHelper.getApp().get(PrismaService);
    await prisma.game.deleteMany();
  });

  afterEach(async () => {
    await appTestHelper.terminate();
  });

  describe('get-games (Handler)', () => {
    it('returns games response', done => {
      prisma.game
        .createMany({
          data: GameTestHelper.getMockGames().map(obj => ({
            ...obj,
            createdDate: new Date(),
          })),
        })
        .then(() => {
          const response: Observable<any> = appTestHelper
            .getNatsClient()
            .send('get-games', {});

          response.subscribe(response => {
            expect(response.message).toEqual('get-games OK');
            expect(response).toHaveProperty('result');
            expect(response.result).toHaveLength(5);

            response.result.forEach(game => {
              expect(game).toHaveProperty('id');
              expect(game).toHaveProperty('name');
              expect(game).toHaveProperty('description');
              expect(game).toHaveProperty('platform');
              expect(game).toHaveProperty('platformGeneration');
              expect(game).toHaveProperty('status');
              expect(game).toHaveProperty('createdDate');
            });

            done();
          });
        })
        .catch(err => {
          throw err;
        });
    });
  });

  describe('create-game (Handler)', () => {
    it('returns game after creating it', done => {
      const gameCreateData = {
        name: 'FIFA 22',
        description: 'Just another FIFA game.',
        platform: [GamePlatform.PLAYSTATION],
        platformGeneration: [GamePlatformGeneration.PS5],
        status: GameStatus.ACTIVE,
      };

      console.log(gameCreateData);

      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('create-game', gameCreateData);

      response.subscribe(response => {
        expect(response.message).toEqual('create-game OK');
        expect(response).toHaveProperty('result');
        expect(response.result).toHaveProperty('id');
        expect(response.result.name).toEqual(gameCreateData.name);

        done();
      });
    });

    it('returns a validation error when passed invalid post message', done => {
      // email property is missing
      const gameCreateData = {
        name: 'some name',
      };

      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('create-game', gameCreateData)
        .pipe(catchError(error => of(error)));

      response.subscribe(response => {
        expect(response.message).toEqual('Validation Error');
        expect(response.result[0]).toEqual(
          'property description has failed the following constraints: description must be a string',
        );

        done();
      });
    });
  });
});
