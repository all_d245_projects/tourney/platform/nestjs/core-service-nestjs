import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '@/app/app.module';
import {
  ClientProxy,
  ClientProxyFactory,
  MicroserviceOptions,
} from '@nestjs/microservices';
import { PrismaService } from '@/app/common/services/prisma/prisma.service';

export class AppTestHelper {
  private app: INestApplication;
  private natsClient: ClientProxy;
  private prisma: PrismaService;

  public async init(testingModule: TestingModule) {
    this.app = this.createApplication(testingModule);
    await this.app.startAllMicroservices();
    await this.app.init();

    this.natsClient = this.app.get('NATS_SERVICE');
    this.prisma = this.app.get(PrismaService);

    await this.natsClient.connect();
  }

  public getApp(): INestApplication {
    return this.app;
  }

  public getNatsClient(): ClientProxy {
    return this.natsClient;
  }

  public getPrismaService(): PrismaService {
    return this.prisma;
  }

  public async terminate() {
    await this.prisma.$disconnect();
    await this.app.close();
    this.natsClient.close();
  }

  static async createE2eTestingModule(): Promise<TestingModule> {
    return await Test.createTestingModule({
      imports: [AppModule],
      providers: [
        {
          provide: 'NATS_SERVICE',
          useFactory: (configService: ConfigService) => {
            const coreServiceNatsConfig = configService.get('nats.coreService');

            return ClientProxyFactory.create(coreServiceNatsConfig);
          },
          inject: [ConfigService],
        },
      ],
    }).compile();
  }

  private createApplication(testingModule: TestingModule): INestApplication {
    const app: INestApplication = testingModule.createNestApplication();
    const configService: ConfigService =
      testingModule.get<ConfigService>(ConfigService);
    const coreServiceNatsConfig = configService.get('nats.coreService');

    app.useGlobalPipes(
      new ValidationPipe(configService.get('app.globalValidationPipeOptions')),
    );

    app.connectMicroservice<MicroserviceOptions>(coreServiceNatsConfig, {
      inheritAppConfig: true,
    });

    return app;
  }
}
