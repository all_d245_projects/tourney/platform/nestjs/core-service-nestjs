import {
  Game,
  GamePlatform,
  GamePlatformGeneration,
  GameStatus,
} from '@prisma/client';
import { faker } from '@faker-js/faker';
import { UniqueEnforcer } from 'enforce-unique';

export class GameTestHelper {
  static getMockGames(numberOfGames = 5): Game[] {
    const games: Game[] = [];
    const uniqueGameNameEnforcer: UniqueEnforcer = new UniqueEnforcer();

    for (let i = 0; i < numberOfGames; i++) {
      const game = this.getRawGameData(uniqueGameNameEnforcer);

      games.push(game as Game);
    }

    return games;
  }

  private static getRawGameData(
    uniqueGameNameEnforcer: UniqueEnforcer,
  ): Record<string, unknown> {
    const name = uniqueGameNameEnforcer.enforce(() => {
      return faker.lorem.words({ min: 1, max: 3 });
    });
    const description = faker.lorem.sentences(3);
    const platform = faker.helpers.arrayElements([GamePlatform.PLAYSTATION]);
    const platformGeneration = faker.helpers.arrayElements([
      GamePlatformGeneration.PS4,
      GamePlatformGeneration.PS5,
    ]);
    const status = faker.helpers.enumValue(GameStatus);

    return {
      id: faker.string.uuid(),
      name,
      description,
      platform,
      platformGeneration,
      status,
    };
  }
}
